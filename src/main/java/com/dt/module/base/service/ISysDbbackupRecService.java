package com.dt.module.base.service;

import com.dt.module.base.entity.SysDbbackupRec;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-06-25
 */
public interface ISysDbbackupRecService extends IService<SysDbbackupRec> {

}
