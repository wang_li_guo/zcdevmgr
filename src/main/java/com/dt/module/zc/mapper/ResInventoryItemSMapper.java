package com.dt.module.zc.mapper;

import com.dt.module.zc.entity.ResInventoryItemS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-06-27
 */
public interface ResInventoryItemSMapper extends BaseMapper<ResInventoryItemS> {

}
