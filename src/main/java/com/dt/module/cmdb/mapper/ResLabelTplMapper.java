package com.dt.module.cmdb.mapper;

import com.dt.module.cmdb.entity.ResLabelTpl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-04-20
 */
public interface ResLabelTplMapper extends BaseMapper<ResLabelTpl> {

}
