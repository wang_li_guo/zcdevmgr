package com.dt.module.cmdb.mapper;

import com.dt.module.cmdb.entity.ResAttrs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-06-18
 */
public interface ResAttrsMapper extends BaseMapper<ResAttrs> {

}
