package com.dt.module.cmdb.service;

import com.dt.module.cmdb.entity.ResActionItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-04-07
 */
public interface IResActionItemService extends IService<ResActionItem> {

}
