package com.dt.module.zc.service;

import com.dt.module.zc.entity.ResInventory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-05-24
 */
public interface IResInventoryService extends IService<ResInventory> {

}
