package com.dt.module.zc.service;

import com.dt.module.zc.entity.ResInout;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-05-27
 */
public interface IResInoutService extends IService<ResInout> {

}
